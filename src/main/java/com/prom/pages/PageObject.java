package com.prom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class PageObject {
    protected WebDriver driver;

    public PageObject(WebDriver driver){
        this.driver = driver;

        PageFactory.initElements(driver, this);
    }

    String getText(By locator){
        return driver.findElement(locator).getText();
    }

    boolean isDisplayed(By locator){
        return driver.findElement(locator).isDisplayed();
    }
}