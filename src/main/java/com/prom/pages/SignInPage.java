package com.prom.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends PageObject {

    @FindBy(id = "phone_email")
    private WebElement email;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "submit_login_button")
    private WebElement submitLoginButton;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return email.isDisplayed();
    }

    public void fillForm(String email, String password) {
        this.email.clear();
        this.email.sendKeys(email);

        this.password.clear();
        this.password.sendKeys(password);
    }

    public DashBoardPage submit() {
        submitLoginButton.click();
        return new DashBoardPage(driver);
    }
}