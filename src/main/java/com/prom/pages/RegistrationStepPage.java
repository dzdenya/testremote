package com.prom.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class RegistrationStepPage extends PageObject {

    @FindBy(xpath = "//div[@class='x-header__auth-item']")
    private WebElement cabinet;

    @FindBy(xpath = "//span[@class='x-drop-down__value']")
    private WebElement selectActivity;

    @FindBy(xpath = "//li[@class='x-drop-down__list-item js-item']")
    private WebElement activityAlreadySell;

    @FindBy(xpath = "//li[contains(., 'Только начинаю свое дело')]")
    private WebElement activityOnlyStart;

    @FindBy(xpath = "(//li[@class='x-drop-down__list-item js-item'])[3]")
    private WebElement activityAlreadySellNotHere;

    @FindBy(xpath = "(//li[@class='x-drop-down__list-item js-item'])[4]")
    private WebElement activityOnlySee;

    @FindBy(xpath = "(//li[@class='x-drop-down__list-item js-item'])[5]")
    private WebElement activityCreate;

    @FindBy(name = "name")
    private WebElement name;

    @FindBy(name = "phone")
    private WebElement phone;

    @FindBy(xpath = "//div[@data-qaid='enter_cabinet']")
    private WebElement createCabinet;

    RegistrationStepPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return cabinet.isDisplayed();
    }

    public void fillRegistrationStep(String nameCabinet){
        this.selectActivity.click();
        this.activityOnlySee.click();

        this.name.sendKeys(nameCabinet);
        this.phone.sendKeys(getRandomNumber());
    }

    public DashBoardPage createCabinet(){
        this.createCabinet.click();
        return new DashBoardPage(driver);
    }

    private String getRandomNumber() {
        String numberPhone = String.valueOf(System.currentTimeMillis()).substring(1, 10);
        savePhoneNumberUsers(numberPhone);
        return numberPhone;
    }

    private void savePhoneNumberUsers(String numberPhone) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/resources/registered_phone_user.txt", true))) {
            writer.write("\n380" + numberPhone);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}