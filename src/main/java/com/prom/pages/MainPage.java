package com.prom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.NoSuchElementException;

public class MainPage extends PageObject {

    @FindBy(xpath = "//a[@data-qaid='ru_lang']")
    private WebElement langRU;

    @FindBy(xpath = "//a[@data-qaid='auth_element']")
    private WebElement login;

    @FindBy(xpath = "//span[@data-qaid='reg_element']")
    private WebElement hoverRegisterButton;

    @FindBy(xpath = "//a[@data-qaid='reg_as_company_btn']")
    private WebElement registerButton;

    private By mainPageCategoryList = By.xpath("//div[@class='x-categories-view']");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public SignUpPage clickRegister(){
        Actions action = new Actions(driver);
        action.moveToElement(hoverRegisterButton).perform();
        action.click(registerButton).perform();

        return new SignUpPage(driver);
    }

    public boolean isInitialized() {
        return driver.findElement(mainPageCategoryList).isDisplayed();
    }


    public void homePage() {
        driver.get("https://prom.ua");
    }

    public void changeLangToRU() {
        this.langRU.click();
    }

    public void clickLogin() {
        this.login.click();
    }
}