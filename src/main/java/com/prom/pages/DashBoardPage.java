package com.prom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



public class DashBoardPage extends PageObject {

    @FindBy(xpath = "(//button[@data-qaid='action_btn'])[1]")
    private WebElement confirmEmail;

    private By ourCompany = By.xpath("//span[@class='b-sidebar-header__company']");

    @FindBy(xpath = "//div[@class='b-content__header']")
    private WebElement contentHeader;

    @FindBy(xpath = "//button[@class='button__root--2qrP1 button__root--2qrP1 button__stateDisabled--2O7Lo']")
    private WebElement buttonConfirmDisable;

    private By closeOverlay = By.xpath("//span[@data-qaid='close_btn']");

    @FindBy(xpath = "//button[@data-qaid='save_order_btn']")
    private WebElement buttonCreate;

    @FindBy(xpath = "//span[@data-qaid='hide_menu_btn']")
    private WebElement hideMenuButton;

    public DashBoardPage(WebDriver driver) {
        super(driver);
    }

    public void confirmEmail() {
        this.confirmEmail.click();
    }

    public String confirmationCabinet() {
        return getText(ourCompany);
    }

    public String confirmationHeader() {
        return contentHeader.getText();
    }

    public boolean isInitialized() {
        if (driver.findElements(closeOverlay).size() > 0)
            driver.findElement(closeOverlay).click();

        if(driver.findElements(ourCompany).size() > 0) {
            return isDisplayed(ourCompany);

        } else {
            this.hideMenuButton.click();
            return isDisplayed(ourCompany);
        }
    }

    public boolean buttonCreateExist() {
        return buttonCreate.isDisplayed();
    }
}
