package com.prom.helpers;

import com.prom.models.Mailsac;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.get;

public class MailHelper {

    private Response response;
    private String tempMail;
    private String baseUrl = "https://mailsac.com";
    private String TOKEN = "?_mailsacKey=e0LBkzubHZGj2OcJqTfcnCRs197TVmcJnX4hX9QXLA";

    public String createTempMail() {
        tempMail = "tempmail_" + randomEndName() + "@mailsac.com";
        response = get(baseUrl + "/inbox/" + tempMail + TOKEN);
        saveEmailUser(tempMail);
        return tempMail;
    }

    @Test
    private String getMsgId() {
        String url = baseUrl + "/api/addresses/" + tempMail + "/messages" + TOKEN;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Mailsac>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Mailsac>>() {
                });
        List<Mailsac> result = response.getBody();

        for (Mailsac mailsac : result) {
            String[] links = mailsac.getLinks();

            for (String link : links) {
                if (link.startsWith("https://my.prom.ua/account/activate_email")) {
                    return link;
                }
            }
        }
        return null;
    }


    public String activateAccount() {
        String linkActivation = getMsgId();
        while (linkActivation == null){
            try {
                Thread.sleep(2000);
                activateAccount();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return linkActivation;
    }

    private String randomEndName() {
        String symbols = "abcdefghijklmnopqrstuvwxyz";
        String random = new Random().ints(5, 0, symbols.length())
                .mapToObj(symbols::charAt)
                .map(Object::toString)
                .collect(Collectors.joining());
        return random;
    }

    private void saveEmailUser(String email) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/resources/registered_email_user.txt", true))) {
            writer.write("\n" + email);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
