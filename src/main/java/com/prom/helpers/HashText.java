package com.prom.helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashText {
    public static void main(String[] args) {
        System.out.println(sha1(""));
    }

    public static String sha1(String input)  {
        byte[] result = new byte[0];
        try {
            result = MessageDigest.getInstance("SHA1").digest(input.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        StringBuffer sb = new StringBuffer();
        for (byte b : result) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

}
