package com.prom.models;

import java.util.Arrays;
import java.util.Objects;

public class Mailsac {
    private String _id;
    private String[] links;

    public String[] getLinks() {
        return links;
    }

    public void setLinks(String[] links) {
        this.links = links;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "Mailsac{" +
                "_id='" + _id + '\'' +
                ", links=" + Arrays.toString(links) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mailsac mailsac = (Mailsac) o;
        return Objects.equals(_id, mailsac._id) &&
                Arrays.equals(links, mailsac.links);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(_id);
        result = 31 * result + Arrays.hashCode(links);
        return result;
    }
}
