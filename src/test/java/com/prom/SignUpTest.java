package com.prom;

import com.prom.helpers.HashText;
import com.prom.helpers.MailHelper;
import com.prom.pages.DashBoardPage;
import com.prom.pages.MainPage;
import com.prom.pages.RegistrationStepPage;
import com.prom.pages.SignUpPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SignUpTest extends TestBase {

    @Test
    public void signUp(){

        MailHelper mailHelper = new MailHelper();
        String email = mailHelper.createTempMail();

        MainPage mainPage = new MainPage(driver);
        mainPage.clickRegister();

        SignUpPage signUpPage = new SignUpPage(driver);
        assertTrue(signUpPage.isInitialized());

        signUpPage.fillForm(email, HashText.sha1(email));

        RegistrationStepPage registrationStepPage = signUpPage.submit();
        assertTrue(registrationStepPage.isInitialized());

        String nameCabinet = "Some Name";
        registrationStepPage.fillRegistrationStep(nameCabinet);
        DashBoardPage boardPage = registrationStepPage.createCabinet();
        boardPage.confirmEmail();
        assertEquals(nameCabinet, boardPage.confirmationCabinet());

        String linkActivation = mailHelper.activateAccount();

        driver.get(linkActivation);
        assertEquals("Возможности покупателя", boardPage.confirmationHeader());
        driver.navigate().back();
        driver.navigate().refresh();
        assertEquals(nameCabinet, boardPage.confirmationCabinet());

        assertTrue(boardPage.isInitialized());

    }
}
