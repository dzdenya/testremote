package com.prom;

import com.prom.helpers.HashText;
import com.prom.helpers.MailHelper;
import com.prom.models.User;
import com.prom.pages.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SignInTest extends TestBase {

    @DataProvider
    public Iterator<Object[]> iteratorList() throws IOException {
        List<Object[]> list = new ArrayList<Object[]>();
        BufferedReader reader = new BufferedReader(
                new FileReader(
                        new File("src/main/resources/registered_email_user.txt")));
        String line = reader.readLine();
        while (line != null) {
            String[] split = line.split("\n");
            list.add(new Object[]{new User()
                    .withEmail(split[0])
                    .withPassword(split[0])});
            line = reader.readLine();
        }
        return list.iterator();
    }

    @BeforeMethod
    public void preCondition(){
        MainPage mainPage = new MainPage(driver);
        mainPage.homePage();

    }

    @Test(dataProvider = "iteratorList")
    public void signIn(User user){

        MainPage mainPage = new MainPage(driver);
        mainPage.changeLangToRU();
        mainPage.clickLogin();

        SignInPage signInPage = new SignInPage(driver);
        assertTrue(signInPage.isInitialized());

        signInPage.fillForm(user.getEmail(), HashText.sha1(user.getPassword()));

        DashBoardPage dashBoardPage = signInPage.submit();
        assertTrue(dashBoardPage.isInitialized());
        assertEquals("Some Name", dashBoardPage.confirmationCabinet());
        assertTrue(dashBoardPage.buttonCreateExist());
    }
}
